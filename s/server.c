#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>

#define broadCastPort 9000
#define bufferSize 256
#define nameSize 20
#define localHost "127.0.0.1"
#define maximumAccounts 20
#define gameStart 60
#define interval 20
int group_ports = 9090;
#define DELAY 2

struct clientInfo {
    char name[nameSize];
    int sharedFileDescriptor;
    int online;
};

struct group {
    char name[nameSize];
    struct clientInfo* clients[maximumAccounts];
    int port;
    int clientNumbers;
};

struct clientInfo *whoWentOut(struct clientInfo *gamer[], int fdIndex, int gamerNumber) {
    int i;
    for (i = 0; i < gamerNumber; i++)
        if (gamer[i]->sharedFileDescriptor == fdIndex)
            return gamer[i];
    return NULL;
}

void heartBeatChecker(int heartBeatSocket,struct sockaddr_in heartBeatAddr){
    char buff[255];
    memset(buff, '\0', 255);
    socklen_t heartBeatAddrLen = sizeof(heartBeatAddr);
    if(recvfrom(heartBeatSocket,buff,sizeof(buff),0,
                (struct sockaddr*)&heartBeatAddr,&heartBeatAddrLen) > 0)
        return;
} 

void c_write(int port, char msg[]) {
	write(port, msg, strlen(msg));
    write(port, "\n", strlen("\n"));
}

void s_write(char msg[], int e) {
    write(1, msg, strlen(msg));
    write(1, "\n", strlen("\n"));
    if (e)
        exit(1);
}

void s_c_write(int port, char msg[], int e) {
    write(port, msg, strlen(msg));
    write(port, "\n", strlen("\n"));
    write(1, msg, strlen(msg));
    write(1, "\n", strlen("\n"));
    if (e)
        exit(1);
}

void checkForGroups() {
    return;
}

int parseAndDefine(char *buffer, int bufSize) {
    int index = 0;
    char command[bufSize];
    memset(command, '\0', bufSize);
    while (buffer[index] != ' ' && index != bufSize)
    {
        command[index] = buffer[index];
        index++;
    }
    if (areEqual(command, "listg"))
        return 6;
    if (index == bufSize)
        return -1;
    if (areEqual(command, "addg"))
        return 1;
    else if (areEqual(command, "chat"))
        return 3;
    else if (areEqual(command, "create"))
        return 2;
    else if (areEqual(command, "joing"))
        return 4;
    else if (areEqual(command, "deleteg"))
        return 5;
    else
        return -1;
}

int areEqual(char *string1, char *string2) {
    int string1Size, string2Size;
    int index;
    string1Size = strlen(string1);
    string2Size = strlen(string2);
    if (string1Size != string2Size)
        return 0;

    for (index = 0; index < string1Size; index++)
        if (string1[index] != string2[index])
            return 0;

    return 1;
}

int isClientOnline(struct clientInfo *gamer[], int fdIndex, int gamerNumber) {
    int index;
    for (index = 0; index < gamerNumber; index++)
        if (gamer[index]->sharedFileDescriptor == fdIndex)
            if (gamer[index]->online == 1)
                return 1;
    return 0;
}

char *extractUsername(char *buffer, int bufSize) {
    char *username;
    int index = 0;
    int index1;

    username = (char *)malloc(nameSize);
    memset(username, '\0', nameSize);

    while (buffer[index] != ' ' && index != bufSize)
        index++;
    if (index == bufSize)
        return NULL;
    index++;
    index1 = index;
    while (buffer[index] != ' ' && index != bufSize && index - index1 < 20)
    {
        username[index - index1] = buffer[index];
        index++;
    }
    if (index == index1)
        return NULL;

    return username;
}

struct clientInfo *findUserByNameByName(char *buffer, int bufSize, struct clientInfo *gamer[], int gamerNumber) {
    char *userPart;
    int index;

    userPart = extractUsername(buffer, bufSize);
    if (userPart == NULL)
        return NULL;

    for (index = 0; index < gamerNumber; index++)
        if (areEqual(userPart, gamer[index]->name))
            return gamer[index];
    return NULL;
}

struct clientInfo *findUserByFD(int fdIndex, struct clientInfo *gamer[], int gamerNumber) {
    int index;
    for (index = 0; index < gamerNumber; index++)
        if (fdIndex == gamer[index]->sharedFileDescriptor)
            return gamer[index];
    return NULL;
}

char *concateString(char *string1, char *string2) {
    char *concated;
    int index;
    int index1;
    concated = (char *)malloc(strlen(string1) + strlen(string2));
    index = 0;
    index1 = 0;
    while (index != strlen(string1))
    {
        concated[index] = string1[index];
        index++;
    }
    while (index1 != strlen(string2))
    {
        concated[index + index1] = string2[index1];
        index1++;
    }
    return concated;
}

void copyStrings(char *string1, char *string2) {
    int index = 0;
    while (string2[index] != '\0')
    {
        string1[index] = string2[index];
        index++;
    }
}

int createAccount(char *buffer, int bufSize, struct clientInfo *clients[], int clientNumber, int fdIndex) {
    char username[nameSize];
    char *userPart;
    int index;

    memset(username, '\0', nameSize);

    if (clientNumber == maximumAccounts)
        return -3;

    userPart = extractUsername(buffer, bufSize);

    if (userPart == NULL)
        return -1;
    copyStrings(username, userPart);
    free(userPart);
    for (index = 0; index < clientNumber; index++)
        if (areEqual(username, clients[index]->name))
            return -2;

    struct clientInfo *newClient;
    newClient = (struct clientInfo *)malloc(sizeof(struct clientInfo));
    copyStrings(newClient->name, username);
    newClient->online = 0;
    clients[clientNumber] = newClient;
    return 1;
}

int createGroup(char* buffer, int bufSize, struct group *groups[], int groupNumber) {
    char groupName[nameSize];
    char *groupnamePart;
    int index;
    memset(groupName, '\0', nameSize);
    if (groupNumber == maximumAccounts-1)
        return -3;
    groupnamePart = extractUsername(buffer, bufSize);
    if (groupnamePart == NULL)
        return -1;
    copyStrings(groupName, groupnamePart);
    free(groupnamePart);
    for (index = 0; index < groupNumber; index++)
        if (areEqual(groupName, groups[index]->name))
            return -2;
    write(1,"hi",strlen("hi"));
    struct group *newGroup;
    newGroup = (struct group *)malloc(sizeof(struct group));
    copyStrings(newGroup->name, groupName);
    group_ports ++;
    newGroup->port = group_ports;
    newGroup->clientNumbers = 0;
    groups[groupNumber] = newGroup;
    return 1;
}

int getGroupPort(char* buffer, int bufSize, struct group *groups[], int groupNumber) {
    char groupName[nameSize];
    char *groupnamePart;
    int index;
    memset(groupName, '\0', nameSize);
    groupnamePart = extractUsername(buffer, bufSize);
    copyStrings(groupName, groupnamePart);
    free(groupnamePart);
    for (int i = 0; i < groupNumber; i++)
        if(areEqual(groupName, groups[i]->name))
            return groups[i]->port;
    return -1;
}

struct clientInfo *login(char *buffer, int bufSize, struct clientInfo *client[], int clientNumber) {
    char username[nameSize];
    char *userPart;
    int index;
    memset(username, '\0', nameSize);
    userPart = extractUsername(buffer, bufSize);
    if (userPart == NULL)
        return NULL;
    copyStrings(username, userPart);
    free(userPart);
    for (index = 0; index < clientNumber; index++)
        if (areEqual(username, client[index]->name))
            return client[index];
    return NULL;
}

int main(int argc, char* argv[]) {
    //Listen to heartbeat broadcast
    int heartBeatSocket;
    struct sockaddr_in heartBeatAddr;
    struct timeval checkServerDelay;

    int yes = 1;
    unsigned short heartBeatPort = atoi(argv[1]);
    if ((heartBeatSocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        write(1, "Could not stablish a connection for broadcasting\n", strlen("Could not stablish a connection for broadcasting\n"));
        exit(0);
    }
    setsockopt(heartBeatSocket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
    checkServerDelay.tv_sec = DELAY;
    checkServerDelay.tv_usec = 0;
    setsockopt(heartBeatSocket, SOL_SOCKET, SO_RCVTIMEO, &checkServerDelay,sizeof(checkServerDelay));

    heartBeatAddr.sin_family = AF_INET;
    heartBeatAddr.sin_addr.s_addr = INADDR_ANY;
    heartBeatAddr.sin_port = htons(heartBeatPort);
    unsigned int heartBeatAddrLen = sizeof(heartBeatAddr);
    if (bind(heartBeatSocket, (struct sockaddr *)&heartBeatAddr, sizeof(heartBeatAddr)) < 0){
        write(1,"Could not bind to that port for listening\n", strlen("Could not bind to that port for listening\n"));
    }
    // finish heartBeat init
	struct sockaddr_in serverAddress, clientAddress;
    int serverSocket, newSocket;
    socklen_t clientAddrLen;
    int readRetValue;
    char *concated;
    fd_set master;
    fd_set readfd;
    int maximumFD;
    int fdIndex;
    int index;
    int parseResult;
    int createResult;
    int gamerNumber = 0;
    int groupNumber = 0;
    struct clientInfo *loginResult;
    struct clientInfo *chatResult;
    struct clientInfo *myInfo;
    struct clientInfo *gamer[maximumAccounts];
    struct clientInfo *disconnected;
    struct group *groups[maximumAccounts];
    struct timeval selectTimer;
    selectTimer.tv_sec = 0;
    selectTimer.tv_usec = 0;
    int connection = 1;

	int firstWarning = 0;
    // int secondWarning = 0;

	FD_ZERO(&master);
    FD_ZERO(&readfd);

	char buffer[bufferSize];
    memset(buffer, '\0', bufferSize);

	if (argc < 2)
        s_write("Not Enough Arguments", 1);

	//Defining Broadcast Part
    int broadCastSocket;
    struct sockaddr_in broadCastAddress;

	if ((broadCastSocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        s_write("Not Enough Arguments", 1);
    
    broadCastAddress.sin_family = AF_INET;
    broadCastAddress.sin_addr.s_addr = INADDR_BROADCAST;
    broadCastAddress.sin_port = htons(broadCastPort);

    setsockopt(broadCastSocket, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes));

	// server
	serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = inet_addr(localHost);
    serverAddress.sin_port = htons(atoi(argv[1]));
    memset(serverAddress.sin_zero, '\0', sizeof(serverAddress.sin_zero));

    if ((serverSocket = socket(PF_INET, SOCK_STREAM, 0)) < 0)
        s_write("No socket Reserved for server", 1);

    if ((bind(serverSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress))) < 0)
        s_write("Could not bind to that port", 1);

    listen(serverSocket, maximumAccounts);

	FD_SET(serverSocket, &master);
    FD_SET(0, &master);
    maximumFD = serverSocket;

	clientAddrLen = sizeof(clientAddress);
	s_write(concateString("server running on port ", argv[1]), 0);

	while(1) {
		readfd = master;
        if (select(maximumFD + 1, &readfd, NULL, NULL, &selectTimer) < 0)
            s_write("select()", 1);
        
		for (fdIndex = 0; fdIndex <= maximumFD; fdIndex++)
        {
            if (FD_ISSET(fdIndex, &readfd))
            {
                if (fdIndex == 0){
                    memset(buffer, '\0', sizeof(buffer));
                    readRetValue = read(fdIndex, buffer, sizeof(buffer));
                    buffer[readRetValue - 1] = '\0';
                    if (areEqual(buffer, "exit()"))
                    {
                        sendto(broadCastSocket, "I was Terminated by hand.Goodbye...\n",
                               strlen("I was Terminated by hand.Goodbye...\n"), 0,
                               (struct sockaddr *)&broadCastAddress, sizeof(broadCastAddress));
                        close(newSocket);
                        close(serverSocket);
                        exit(1);
                    }
					else
						s_write("wrong command.", 0);
                }
                else if (fdIndex == serverSocket){
                    newSocket = accept(serverSocket, (struct sockaddr *)&clientAddress, &clientAddrLen);
                    if (newSocket < 0)
                    {
                        s_write("Connection Failed", 0);
                    }
                    else {
                        FD_SET(newSocket, &master);
						s_write("new user added.", 0);
					}
                    if (maximumFD < newSocket)
                        maximumFD = newSocket;
                }
				else {
					memset(buffer, '\0', bufferSize);
                    readRetValue = read(fdIndex, buffer, bufferSize);

                    if (readRetValue == 0)
                    {
                        disconnected = whoWentOut(gamer, fdIndex, gamerNumber);
                        if (disconnected != NULL)
                        {
                            disconnected->online = 0;
                            disconnected->sharedFileDescriptor = -1;
                            s_write(concateString(disconnected->name, " exited."), 0);
                        }
                        else
                            s_write("Connection Lost", 0);
                        FD_CLR(fdIndex, &master);
                        close(fdIndex);
                    }
                    else {
                        s_write(concateString("client: ", buffer), 0);
                        parseResult = parseAndDefine(buffer, strlen(buffer));
                    }
                    if (parseResult == -1)
                        c_write(fdIndex, "Wrong Format");
                    else if (parseResult == 2) {
                        if (isClientOnline(gamer, fdIndex, gamerNumber)) {
                            c_write(fdIndex, "you are logged in");
                            continue;
                        }
                        createResult = createAccount(buffer, strlen(buffer), gamer, gamerNumber, fdIndex);
                        if (createResult == -1)
                            c_write(fdIndex, "Wrong Format Entered");
                        else if (createResult == -2)
                            c_write(fdIndex, "An account has been created by this username.");

                        else if (createResult == -3)
                            c_write(fdIndex, "Enough players have created account");

                        else if (createResult == 1)
                        {
                            if (gamerNumber < maximumAccounts)
                                gamerNumber++;
                            s_c_write(fdIndex, "You have Created your account:)", 0);
                        }
                        loginResult = login(buffer, strlen(buffer), gamer, gamerNumber);
                        if (loginResult == NULL)
                            c_write(fdIndex, "Wrong format or not created");
                        else
                        {
                            if (loginResult->online == 1)
                            {
                                c_write(fdIndex, "This Account is online");
                                continue;
                            }
                            loginResult->online = 1;
                            loginResult->sharedFileDescriptor = fdIndex;
                            s_write(concateString("new client name: ", loginResult->name), 0);
                            c_write(fdIndex, concateString("You have Logged in as ", loginResult->name));
                        }
                    }
                    else if (parseResult == 3)
                    {
                        chatResult = findUserByNameByName(buffer, strlen(buffer), gamer, gamerNumber);
                        myInfo = findUserByFD(fdIndex, gamer, gamerNumber);
                        if (myInfo == NULL)
                        {
                            c_write(fdIndex, "You have not loggined yet");
                            continue;
                        }
                        if (chatResult == NULL)
                        {
                            c_write(fdIndex, "Wrong Format or Not Existing");
                            continue;
                        }
                        if (areEqual(chatResult->name, myInfo->name))
                        {
                            c_write(fdIndex, "You cannot chat with yourself!");
                            continue;
                        }
                        else if (chatResult->online == 0)
                            c_write(fdIndex, "Your Contact is offline");
                        else if (chatResult->online == 1)
                        {
                            write(fdIndex, "Trying to connect you to your contact...\n", strlen("Trying to connect you to your contact...\n"));
                            write(chatResult->sharedFileDescriptor, "Port#", strlen("Port#"));
                            memset(buffer, '\0', bufferSize);
                            readRetValue = read(chatResult->sharedFileDescriptor, buffer, bufferSize);
                            buffer[readRetValue] = '\0';
                            concated = concateString("Chat#", buffer);
                            write(myInfo->sharedFileDescriptor, concated, strlen(concated));
                            free(concated);
                        }
                    }
                    else if (parseResult == 1) {
                        createResult = 0;
                        createResult = createGroup(buffer, strlen(buffer), groups, groupNumber);
                        if (createResult == -1)
                            c_write(fdIndex, "Wrong Format Entered");
                        else if (createResult == -2)
                            c_write(fdIndex, "An account has been created by this username.");
                        else if (createResult == -3)
                            c_write(fdIndex, "Enough players have created account");
                        else if (createResult == 1) {
                            groupNumber ++;
                            s_c_write(fdIndex, "new group has beenn added", 0);
                        }
                    }
                    else if (parseResult == 4) {
                        int port = getGroupPort(buffer, strlen(buffer), groups, groupNumber);
                        if(port > 9089) {
                            char c[50];
                            sprintf(c, "%d", port);
                            c[4] = '\0';
                            char * portInfo;
                            s_c_write(fdIndex, concateString("gport#", c), 0);
                        }
                        else
                            s_c_write(fdIndex, "group not found.", 0);
                    }
                    else if (parseResult == 5) {
                        write(1, "deleteG\n", strlen("addG\n"));
                        write(fdIndex, "deleteG\n", strlen("addG\n"));
                    }
                    else if (parseResult == 6) {
                        for (int i = 0; i < groupNumber; i++)
                            c_write(fdIndex, groups[i]->name);
                    }
				}
            }
            // heartBeatChecker(heartBeatSocket, heartBeatAddr);
        }
	}
	return 0;
}