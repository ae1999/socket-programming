#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>

#define broadCastPort 9000
#define HBP 9098
#define bufferSize 256
#define nameSize 20
#define localHost "127.0.0.1"
#define DELAY 2

int heartbeatSock;
struct sockaddr_in broadCastAddr;

void heartBeatHandler(int signum, int groups){
    char heartBeatMassage[bufferSize];
    if (signum == SIGALRM)
    {
        if ((sendto(heartbeatSock, heartBeatMassage, strlen(heartBeatMassage),
         0, (struct sockaddr *) &broadCastAddr, sizeof(broadCastAddr))) < 0){
            write(1,"Heartbeat sending failed\n",strlen("Heartbeat sending failed\n"));  
            exit(EXIT_FAILURE);
        }   
        alarm(DELAY);
    }
}

char *concateString(char *string1, char *string2) {
    char *concated;
    int index;
    int index1;
    concated = (char *)malloc(strlen(string1) + strlen(string2));
    index = 0;
    index1 = 0;
    while (index != strlen(string1))
    {
        concated[index] = string1[index];
        index++;
    }
    while (index1 != strlen(string2))
    {
        concated[index + index1] = string2[index1];
        index1++;
    }
    return concated;
}

int areEqual(char *string1, char *string2) {
    int string1Size, string2Size;
    int index;
    string1Size = strlen(string1);
    string2Size = strlen(string2);
    if (string1Size != string2Size)
        return 0;

    for (index = 0; index < string1Size; index++)
        if (string1[index] != string2[index])
            return 0;

    return 1;
}

char *extractPort(char *buffer, int bufSize) {
    char *port;
    port = (char *)malloc(bufSize - 5);
    int index = 0;
    int index1;
    memset(port, '\0', bufSize);
    while (buffer[index] != '#' && index != bufSize)
        index++;
    if (index == bufSize)
        return NULL;
    index++;
    index1 = index;
    while (index != bufSize)
    {
        port[index - index1] = buffer[index];
        index++;
    }
    return port;
}

char *extractUsername(char *buffer, int bufSize) {
    char *username;
    int index = 0;
    int index1;

    username = (char *)malloc(nameSize);
    memset(username, '\0', nameSize);

    while (buffer[index] != ' ' && index != bufSize)
        index++;
    if (index == bufSize)
        return NULL;
    index++;
    index1 = index;
    while (buffer[index] != ' ' && index != bufSize && index - index1 < 20)
    {
        username[index - index1] = buffer[index];
        index++;
    }
    if (index == index1)
        return NULL;

    return username;
}

int parseCommand(char *buffer, int bufSize) {
    char message[bufSize];
    int index = 0;
    memset(message, '\0', strlen(message));
    while (buffer[index] != '#' && index != bufSize)
    {
        message[index] = buffer[index];
        index++;
    }
    if (index == bufSize)
        return -1;
    if (areEqual(message, "Port"))
        return 1;
    if (areEqual(message, "Chat"))
        return 2;
    if (areEqual(message, "gport"))
        return 3;
    if (areEqual(message, "exitg"))
        return 3;
    return -1;
}

int powerOfTen(int power) {
    int base = 1;
    while (power != 0)
    {
        base = base * 10;
        power--;
    }
    return base;
}

int convertToInteger(char *buffer, int bufSize) {
    int sum = 0;
    int index;
    for (index = bufSize - 1; index >= 0; index--)
        sum = sum + (buffer[index] - 48) * powerOfTen(bufSize - index - 1);
    return sum;
}

int findMaximum(int a,int b) {
    if(a > b)
        return a;
    else
        return b;
}

void chat(int fd, char *username,int broadCastSocket) {
    fd_set master;
    fd_set readfd;
    int fdIndex;
    int maximumFd;
    int readRetValue;
    char buffer[bufferSize];
    int connection = 1;
    struct sockaddr_in broadCasterAddress;
    socklen_t broadCasterLen;

    broadCasterLen = sizeof(broadCasterAddress);

    FD_ZERO(&master);
    FD_ZERO(&readfd);
    FD_SET(0, &master);
    FD_SET(broadCastSocket,&master);
    FD_SET(fd, &master);
    maximumFd = findMaximum(fd,broadCastSocket);

    while (connection)
    {
        readfd = master;
        if (select(maximumFd + 1, &readfd, NULL, NULL, NULL) < 0)
        {
            write(1, "Chatting could not select.\n", strlen("Chatting could not select.\n"));
            continue;
        }

        for (fdIndex = 0; fdIndex <= maximumFd; fdIndex++)
        {
            if (FD_ISSET(fdIndex, &readfd))
            {
                if (fdIndex == 0)
                {
                    memset(buffer, '\0', bufferSize);
                    readRetValue = read(0, buffer, bufferSize);
                    buffer[readRetValue - 1] = '\0';
                    if (readRetValue == 0)
                    {
                        write(1, "Reading from terminal Failed...\n", strlen("Reading from terminal Failed...\n"));
                        continue;
                    }
                    if (areEqual(buffer, "exit"))
                    {
                        write(fd, username, strlen(username));
                        write(fd, " :", strlen(" :"));
                        write(fd, "I am leaving.Goodbye.\n", strlen("I am leaving.Goodbye.\n"));
                        connection = 0;
                        close(fd);
                        break;
                    }
                    buffer[readRetValue - 1] = '\n';
                    write(fd, username, strlen(username));
                    write(fd, " :", strlen(" :"));
                    write(fd, buffer, strlen(buffer));
                }
                else if(fdIndex == broadCastSocket){
                    memset(buffer,'\0',bufferSize);
                    readRetValue = recvfrom(broadCastSocket,buffer,bufferSize,0,
                    (struct sockaddr*)&broadCasterAddress,&broadCasterLen);
                    if(readRetValue == 0){
                        write(1, "Error getting server interrupt\n", strlen("Error getting server interrupt\n"));
                        exit(0);
                    }
                    if(areEqual(buffer,"Interrupt")){
                        write(1, "Server is interrupting me !\n", strlen("Server is interrupting me !\n"));
                        connection = 0;
                        break;
                    }
                }
                else
                {
                    memset(buffer, '\0', bufferSize);
                    readRetValue = read(fdIndex, buffer, bufferSize);
                    if (readRetValue == 0)
                    {
                        write(1, username, strlen(username));
                        write(1, " :Terminating chat.\n", strlen(" :Terminating chat.\n"));
                        connection = 0;
                        break;
                    }
                    write(1, buffer, strlen(buffer));
                }
            }
        }
    }
}

void groupChat(int port) {
    //Initializing broadcast socket for heartbeat
    unsigned short heartBeatPort;
    int yes = 1;
    heartBeatPort = HBP;
    if ((heartbeatSock = socket(AF_INET,SOCK_DGRAM,0)) < 0 )
    {
        write(1, "Server failed to stablish broadcasting\n", strlen("Server failed to stablish broadcasting\n"));
        exit(0);
    }
    broadCastAddr.sin_family = AF_INET;
    broadCastAddr.sin_addr.s_addr = INADDR_BROADCAST;
    broadCastAddr.sin_port = htons(heartBeatPort);
    setsockopt(heartbeatSock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes));
    signal(SIGALRM,heartBeatHandler);
    alarm(DELAY);
    //Finish initializing broadcast socket for heartbeat

    struct sockaddr_in serverAddress;
    int serverSocket;
    socklen_t clientAddrLen;
    int readRetValue;
    fd_set master;
    fd_set readfd;
    int maximumFD;
    int fdIndex;
    int index;
    struct timeval selectTimer;
    selectTimer.tv_sec = 0;
    selectTimer.tv_usec = 0;
    FD_ZERO(&master);
    FD_ZERO(&readfd);

	char buffer[bufferSize];
    memset(buffer, '\0', bufferSize);

	// server
	memset(&serverAddress, 0, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = inet_addr("127.0.0.1");
    serverAddress.sin_port = htons(port);

	printf("%s\n", "Group port: ");
	printf("%d\n", ntohs(serverAddress.sin_port));
    printf("%s\n", "Group: ");

    if ((serverSocket = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
    	write(1, "No socket Reserved for server", strlen("No socket Reserved for server"));

	setsockopt(serverSocket, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(yes));

    if ((bind(serverSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress))) < 0)
		perror("(");

    //listen(serverSocket, maximumAccounts);

	FD_SET(serverSocket, &master);
    FD_SET(0, &master);
    maximumFD = serverSocket;

	while(1) {
		readfd = master;
        if (select(maximumFD + 1, &readfd, NULL, NULL, &selectTimer) < 0)
            write(1, "select()", 8);
        
		for (fdIndex = 0; fdIndex <= maximumFD; fdIndex++)
        {
            if (FD_ISSET(fdIndex, &readfd))
            {
                if (fdIndex == 0){
                    memset(buffer, '\0', sizeof(buffer));
                    readRetValue = read(fdIndex, buffer, sizeof(buffer));
                    buffer[readRetValue] = '\0';
                    if (areEqual(buffer, "exit()"))
                        return;
					else {
						if((sendto(serverSocket, buffer, readRetValue, 0, (struct sockaddr *) &serverAddress, sizeof(serverAddress))) != strlen(buffer))
                        {
                            perror("sendto");
                            exit(0);
                        }
                    }
                }
                else if (fdIndex == serverSocket){
					memset(buffer, '\0', bufferSize);
                    recvfrom(serverSocket, buffer, bufferSize, 0, NULL, 0);
                    printf("%s\n",buffer);
                }
            }
        }
	}
}

int main(int argc, char *argv[])
{
    struct sockaddr_in serverAddress;
    struct sockaddr_in clientAddr, myServerAddress, targetChatAddress;
    int myServerSocket, newSocket;
    socklen_t clientAddrLen;
    int clientSocket;
    int chatSocket;
    int readRetValue;
    char username[nameSize];
    char *port;
    fd_set master;
    fd_set readfd;
    int maximumFd;
    int fdIndex;
    int index;
    int connection = 1;
    int groups[10];
    int gnum = 0;

    char buffer[bufferSize];
    memset(buffer, '\0', bufferSize);
    if (argc < 3)
    {
        write(1, "Arguments format is not valid\n", strlen("Arguments format is not valid\n"));
        exit(1);
    }

    //Broad Cast Part definition
    struct sockaddr_in myBroadCastAddress, broadCasterAddress;
    int listenSocket;
    socklen_t broadCasterLen;
    int yes = 1;

    if ((listenSocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        write(1, "Could not stablish a connection for broadcasting\n", strlen("Could not stablish a connection for broadcasting\n"));
        exit(0);
    }
    setsockopt(listenSocket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));

    myBroadCastAddress.sin_family = AF_INET;
    myBroadCastAddress.sin_addr.s_addr = INADDR_ANY;
    myBroadCastAddress.sin_port = htons(broadCastPort);

    // if (bind(listenSocket, (struct sockaddr *)&myBroadCastAddress, sizeof(myBroadCastAddress)) < 0)
    // {
    //     write(1, "Could not bind to that port to broadcast\n", strlen("Could not bind to that port to broadcast\n"));
    //     exit(0);
    // }
    broadCasterLen = sizeof(broadCasterAddress);
    //Broad cast Part Definition

    //Defining Server Address
    serverAddress.sin_addr.s_addr = inet_addr(localHost);
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(atoi(argv[1]));
    memset(serverAddress.sin_zero, '\0', sizeof(serverAddress.sin_zero));

    //Defining My Server Address
    myServerAddress.sin_family = AF_INET;
    myServerAddress.sin_addr.s_addr = inet_addr(localHost);
    myServerAddress.sin_port = htons(atoi(argv[2]));
    memset(myServerAddress.sin_zero, '\0', sizeof(myServerAddress.sin_zero));

    //Defining Chat Server Address
    targetChatAddress.sin_addr.s_addr = inet_addr(localHost);
    targetChatAddress.sin_family = AF_INET;
    memset(targetChatAddress.sin_zero, '\0', sizeof(targetChatAddress.sin_zero));

    if ((myServerSocket = socket(PF_INET, SOCK_STREAM, 0)) < 0)
    {
        write(1, "Could not open a socket for server\n", strlen("Could not open a socket for server\n"));
        exit(1);
    }

    // if (bind(myServerSocket, (struct sockaddr *)&myServerAddress, sizeof(myServerAddress)) < 0)
    // {
    //     write(1, "Could not bind to that port\n", strlen("Could not bind to that port\n"));
    //     exit(1);
    // }

    listen(myServerSocket, 5);
    clientAddrLen = sizeof(clientAddr);

    if ((chatSocket = socket(PF_INET, SOCK_STREAM, 0)) < 0)
    {
        write(1, "Could not open a socket for chatting\n", strlen("Could not open a socket for chatting\n"));
        exit(1);
    }

    if ((clientSocket = socket(PF_INET, SOCK_STREAM, 0)) < 0)
    {
        write(1, "Could not open a socket for client\n", strlen("Could not open a socket for client\n"));
        exit(1);
    }

    FD_ZERO(&master);
    FD_ZERO(&readfd);
    FD_SET(myServerSocket, &master);
    FD_SET(listenSocket,&master);
    FD_SET(clientSocket, &master);
    FD_SET(0, &master);
    maximumFd = clientSocket;

    write(1, "Client : Trying to connect...\n", strlen("Client : Trying to connect...\n"));
    if (connect(clientSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0)
    {
        write(1, "Connection Failed.\n", strlen("Connection Failed.\n"));
        exit(1);
    }
    write(1, "Connection Stablished!\n", strlen("Connection Stablished!\n"));

    while (connection)
    {
        readfd = master;
        if (select(maximumFd + 1, &readfd, NULL, NULL, NULL) < 0)
        {
            write(1, "Client : Selecting Problem...\n", strlen("Client : Selecting Problem...\n"));
            exit(1);
        }

        for (fdIndex = 0; fdIndex <= maximumFd; fdIndex++)
        {
            if (FD_ISSET(fdIndex, &readfd))
            {
                if (fdIndex == myServerSocket)
                {
                    newSocket = accept(myServerSocket, (struct sockaddr *)&clientAddr, &clientAddrLen);
                    if (newSocket < 0)
                    {
                        write(1, "Could not stablish a connection for chat\n", strlen("Could not stablish a connection for chat\n"));
                        close(newSocket);
                        continue;
                    }
                    write(1, "Connected as server for chatting\n", strlen("Connected as server for chatting\n"));
                    chat(newSocket,"Server",listenSocket);
                    close(newSocket);
                }
                else if (fdIndex == 0)
                {
                    memset(buffer, '\0', bufferSize);
                    readRetValue = read(0, buffer, bufferSize);
                    buffer[readRetValue - 1] = '\0';
                    if (areEqual(buffer, "exit"))
                    {
                        connection = 0;
                        break;
                    }
                    if (areEqual(buffer, "exitg"))
                    {
                        memset(buffer, '\0', bufferSize);
                        readRetValue = read(0, buffer, bufferSize);
                        int ep = atoi(buffer);
                        printf("%d\n", ep);
                        for(int i = 0; i < gnum; i++) {
                            if(ep == groups[i]) {
                                for( int j = i; j < gnum; j++)
                                    groups[j] = groups[j+1];
                                break;
                            }
                        }
                        for(int i = 0; i < gnum; i++) 
                            printf("%d\n", groups[i]);
                        continue;
                    }
                    write(clientSocket, buffer, strlen(buffer));
                }
                else if(fdIndex == listenSocket){
                    memset(buffer,'\0',bufferSize);
                    readRetValue = recvfrom(listenSocket,buffer,sizeof(buffer),0,
                    (struct sockaddr*)&broadCasterAddress,&broadCasterLen);
                    if(readRetValue == 0){
                        write(1, "Connection lost! Exiting\n", strlen("Connection lost! Exiting\n"));
                        exit(0);
                    }
                    buffer[readRetValue] = '\n';
                    write(1,buffer,strlen(buffer));
                }
                else
                {
                    memset(buffer, '\0', bufferSize);
                    readRetValue = read(clientSocket, buffer, bufferSize);
                    if (readRetValue == 0)
                    {
                        write(1, "Server went down. Exiting...\n", strlen("Server went down. Exiting...\n"));
                        exit(1);
                    }
                    if (parseCommand(buffer, strlen(buffer)) == 1)
                    {
                        write(fdIndex, argv[2], strlen(argv[2]));
                        continue;
                    }
                    if (parseCommand(buffer, strlen(buffer)) == 2)
                    {
                        port = extractPort(buffer, strlen(buffer));
                        targetChatAddress.sin_port = htons(convertToInteger(port, strlen(port)));
                        if (connect(chatSocket, (struct sockaddr *)&targetChatAddress, sizeof(targetChatAddress)) < 0)
                        {
                            write(1, "Could not connect for chatting...\n", strlen("Could not connect for chatting...\n"));
                            continue;
                        }
                        write(1, "Connected as client for chatting\n", strlen("Connected as client for chatting\n"));
                        memset(username,'\0',sizeof(username));
                        chat(chatSocket,"Client",listenSocket);
                        continue;
                    }
                    if (parseCommand(buffer, strlen(buffer)) == 3)
                    {
                        port = extractPort(buffer, strlen(buffer));
                        int p = atoi(port);
                        groups[gnum] = p;
                        gnum ++;
                        groupChat(p);
                        continue;
                    }
                    buffer[readRetValue] = '\n';
                    write(1, buffer, strlen(buffer));
                }
            }
        }
    }

    close(clientSocket);
    exit(1);
}